# A SeeSaw Game made via Vue.js

<a href="https://pooria.han.gitlab.io/vue-seesaw/">
    <b>
        Demo
    </b>
</a>

------


## Setup

1. Run `npm i` once to install packages.
2. Run `npm run serve` for development or `npm run build` for production.


### Links
- [Vue.js documentation](https://vuejs.org/v2/guide/)
- [Vue CLI documentation](https://cli.vuejs.org/guide/)
