export default {
  generateRandomObject(state) {
    state.randomObject.weight = Math.ceil(Math.random() * 10);
    state.randomObject.shape = state.shapes[Math.floor(Math.random() * state.shapes.length)];
  },
  generateRandomDistance(state) {
    state.randomObject.distanceFromCenter = Math.floor(Math.random() * (5 + 1));
  },
  calculateRightSideTotalWeight(state) {
    if (state.randomObject.distanceFromCenter === 0) {
      state.rightSideTotalWeight = state.rightSideTotalWeight + state.randomObject.weight;
    } else {
      state.rightSideTotalWeight = state.rightSideTotalWeight + (state.randomObject.weight * state.randomObject.distanceFromCenter);
    }
  },
  calculateLeftSideTotalWeight(state, object) {
    if (object.offset === 0) {
      state.leftSideTotalWeight = state.leftSideTotalWeight + object.weight;
    } else {
      state.leftSideTotalWeight = state.leftSideTotalWeight + (object.weight * object.offset);
    }
  },
  calculateBending(state) {
    state.bendingPercentage = state.rightSideTotalWeight + -Math.abs(state.leftSideTotalWeight);
  },
  pushToRightSideObjects(state) {
    const newObject = {
      weight: state.randomObject.weight,
      shape: state.randomObject.shape,
      distanceFromCenter: state.randomObject.distanceFromCenter
    };
    state.rightSideObjects.push(newObject);
  },
  pushToLeftSideObjects(state) {
    const newObject = {
      weight: state.randomObject.weight,
      shape: state.randomObject.shape,
      distanceFromCenter: null,
      landed: false
    };
    state.leftSideObjects.push(newObject);
  },
  setLeftSideTimer(state, newValue) {
    state.leftSideTimer = newValue;
  },
  setRightSideTimer(state, newValue) {
    state.rightSideTimer = newValue;
  },
  clearLeftSideTimer(state) {
    clearInterval(state.leftSideTimer);
  },
  clearRightSideTimer(state) {
    clearInterval(state.rightSideTimer);
  },
  setPlaying(state, newValue) {
    state.playing = newValue;
  },
  setGameOver(state, newValue) {
    state.gameOver = newValue;
  },
  setGameEverStarted(state, newValue) {
    state.gameEverStarted = newValue;
  },
  clearAllOtherStates(state) {
    state.bendingPercentage = 0;
    state.leftSideObjects = [];
    state.leftSideTimer = null;
    state.leftSideTotalWeight = 0;
    state.randomObject = {};
    state.rightSideObjects = [];
    state.rightSideTimer = null;
    state.rightSideTotalWeight = 0;
  }
};
