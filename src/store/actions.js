export default {
  generateRandomRightSideObject({commit}) {
    commit('generateRandomObject');
    commit('generateRandomDistance');
    commit('pushToRightSideObjects');
    commit('calculateRightSideTotalWeight');
    commit('calculateBending');
  },
  generateRandomLeftSideObject({commit}) {
    commit('generateRandomObject');
    commit('pushToLeftSideObjects');
  },
  startTheGame({commit, dispatch}) {
    setTimeout(() => {
      document.activeElement.blur();
    }, 1);

    dispatch('generateRandomLeftSideObject');

    const leftSideTimer = setInterval(function () {
      dispatch('generateRandomLeftSideObject');
    }, 4000);

    const rightSideTimer = setInterval(function() {
      dispatch('generateRandomRightSideObject');
    }, 3000);

    commit('setGameEverStarted', true);
    commit('setPlaying', true);
    commit('setLeftSideTimer', leftSideTimer);
    commit('setRightSideTimer', rightSideTimer);
  },
  pauseTheGame(context) {
    context.dispatch('clearTimers');
    context.commit('setPlaying', false);
  },
  resetTheGame({commit, dispatch}) {
    dispatch('clearTimers');
    commit('setGameOver', false);
    commit('setGameEverStarted', false);
    commit('clearAllOtherStates');
  },
  clearTimers({commit}) {
    commit('clearLeftSideTimer');
    commit('clearRightSideTimer');
  },
  gameOver({ commit, dispatch }) {
    commit('setGameOver', true);
    commit('setPlaying', false);
    dispatch('clearTimers');
  }
};