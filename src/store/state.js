export default {
  playing: false,
  gameOver: false,
  gameEverStarted: false,
  rightSideTotalWeight: 0,
  leftSideTotalWeight:  0,
  rightSideObjects: [],
  leftSideObjects: [],
  bendingPercentage: 0, // between -30 and 30
  randomObject: {
    weight: null,
    shape: '',
    distanceFromCenter: null,
  },
  leftSideTimer: null,
  rightSideTimer: null,
  shapes: [
    'circle',
    'triangle',
    'square'
  ]
};
